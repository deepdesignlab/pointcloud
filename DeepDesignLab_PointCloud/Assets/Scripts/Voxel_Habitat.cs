﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using DeepDesignLab.Base;

namespace DeepDesignLab.PointCloud {

    [Serializable]
    public class Voxel_Habitat : VoxelBase, IEquatable<Voxel_Habitat> {
        // As it is a child of VoxelBase it has a centroid, normal and colour.

            [SerializeField]
        private static int nVoxels;

        // The following are properties of a voxel, they are private so as to not accedently edit them. 
        [SerializeField]
        private readonly int id;
        [SerializeField]
        private int clusterID;
        [SerializeField]
        private int intensity;

        [NonSerialized]
        private List<Voxel_Habitat> neighbours = new List<Voxel_Habitat>();

        public Vector3 offset = Vector3.zero;

        // This is the public accessors. this allows informaiton to go one way.
        public Vector3 getPosition { get { return centroid; } }
        public Vector3 getNormal { get { return normal; } }
        public Color getColour { get { return colour; } }
        public Voxel_Habitat[] getNeighbours { get { return neighbours.ToArray(); } }

        public Vector3 renderPosition { get { return centroid + offset; } }
        public int getID { get { return id; } }


        // This allows the equals oporation to test if the index are the same. 
        // NOT TESTED but should speed things up.
        public override bool Equals(object obj) {
            if (obj == null) return false;
            Voxel_Habitat objAsPart = obj as Voxel_Habitat;
            if (objAsPart == null) return false;
            else return Equals(objAsPart);
        }
        public override int GetHashCode() {
            return id;
        }
        public bool Equals(Voxel_Habitat other) {
            if (other == null) return false;
            return (this.id.Equals(other.id));
        }

        /// <summary>
        /// Create a enw voxel using the CloudCompare data, {X,Y,Z,R,G,B,Scalar Field,Nx,Ny,Nz}
        /// </summary>
        /// <param name="CCtextData"></param> Double array in the form {X,Y,Z,R,G,B,Scalar Field,Nx,Ny,Nz}
        /// <param name="newID"></param> New id, MUST BE UNIQUE!
        public Voxel_Habitat(double[] CCtextData) {
            centroid = new Vector3((float)CCtextData[0], (float)CCtextData[1], (float)CCtextData[2]);
            colour = new Color((float)CCtextData[3] / 255, (float)CCtextData[4] / 255, (float)CCtextData[5] / 255);
            normal = new Vector3((float)CCtextData[7], (float)CCtextData[8], (float)CCtextData[9]);
            intensity = (int)CCtextData[6];
            nVoxels++;
            id = nVoxels;
        }


        /// <summary>
        /// Updates the colour of the base voxel.
        /// </summary>
        /// <param name="newColour"></param>
        public void updateColour(Color newColour) {
            colour = newColour;
        }

        public void setNeighbour(Voxel_Habitat newNeighbour) {
            if (!neighbours.Contains(newNeighbour)) {
                neighbours.Add(newNeighbour);
            }
        }

        public bool isNeighbour(Voxel_Habitat VoxelToCheck) {
            return neighbours.Contains(VoxelToCheck);
        }

    }

}